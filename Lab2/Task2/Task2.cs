﻿using System;

// Простір імен для унікалізації класів
namespace DeviceFactoryExample
{
    // Абстрактний клас для девайсів
    abstract class Device
    {
        public abstract string GetDetails();
    }

    // Конкретні класи девайсів
    class Laptop : Device
    {
        private string brand;
        public Laptop(string brand)
        {
            this.brand = brand;
        }
        public override string GetDetails()
        {
            return $"{brand} Laptop";
        }
    }

    class Netbook : Device
    {
        private string brand;
        public Netbook(string brand)
        {
            this.brand = brand;
        }
        public override string GetDetails()
        {
            return $"{brand} Netbook";
        }
    }

    class EBook : Device
    {
        private string brand;
        public EBook(string brand)
        {
            this.brand = brand;
        }
        public override string GetDetails()
        {
            return $"{brand} EBook";
        }
    }

    class Smartphone : Device
    {
        private string brand;
        public Smartphone(string brand)
        {
            this.brand = brand;
        }
        public override string GetDetails()
        {
            return $"{brand} Smartphone";
        }
    }

    // Абстрактна фабрика
    abstract class DeviceFactory
    {
        public abstract Device CreateLaptop();
        public abstract Device CreateNetbook();
        public abstract Device CreateEBook();
        public abstract Device CreateSmartphone();
    }

    // Конкретні фабрики для брендів
    class IProneFactory : DeviceFactory
    {
        public override Device CreateLaptop()
        {
            return new Laptop("IProne");
        }

        public override Device CreateNetbook()
        {
            return new Netbook("IProne");
        }

        public override Device CreateEBook()
        {
            return new EBook("IProne");
        }

        public override Device CreateSmartphone()
        {
            return new Smartphone("IProne");
        }
    }

    class KiaomiFactory : DeviceFactory
    {
        public override Device CreateLaptop()
        {
            return new Laptop("Kiaomi");
        }

        public override Device CreateNetbook()
        {
            return new Netbook("Kiaomi");
        }

        public override Device CreateEBook()
        {
            return new EBook("Kiaomi");
        }

        public override Device CreateSmartphone()
        {
            return new Smartphone("Kiaomi");
        }
    }

    class BalaxyFactory : DeviceFactory
    {
        public override Device CreateLaptop()
        {
            return new Laptop("Balaxy");
        }

        public override Device CreateNetbook()
        {
            return new Netbook("Balaxy");
        }

        public override Device CreateEBook()
        {
            return new EBook("Balaxy");
        }

        public override Device CreateSmartphone()
        {
            return new Smartphone("Balaxy");
        }
    }

    // Головний клас програми з унікальним іменем
    class ProgramDeviceFactory
    {
        static void Main(string[] args)
        {
            DeviceFactory iproneFactory = new IProneFactory();
            DeviceFactory kiaomiFactory = new KiaomiFactory();
            DeviceFactory balaxyFactory = new BalaxyFactory();

            Device iproneLaptop = iproneFactory.CreateLaptop();
            Device kiaomiSmartphone = kiaomiFactory.CreateSmartphone();
            Device balaxyEBook = balaxyFactory.CreateEBook();

            Console.WriteLine(iproneLaptop.GetDetails());
            Console.WriteLine(kiaomiSmartphone.GetDetails());
            Console.WriteLine(balaxyEBook.GetDetails());
        }
    }
}
