﻿using System;
using System.IO;

public class Authenticator
{
    // Приватне статичне поле для зберігання єдиного екземпляра
    private static Authenticator instance;

    // Об'єкт для блокування
    private static readonly object lockObject = new object();

    // Змінні для прикладу
    private string configValue;
    private StreamWriter logger;

    // Приватний конструктор, щоб запобігти створенню екземплярів ззовні
    private Authenticator()
    {
        // Ініціалізація логера
        logger = new StreamWriter("log.txt", append: true);

        // Завантаження конфігурацій (наприклад, з файлу)
        configValue = LoadConfig();

        // Ініціалізація інших необхідних даних
        Console.WriteLine("Authenticator instance created.");
    }

    private string LoadConfig()
    {
        // Імітація завантаження конфігурації
        return "ConfigValue";
    }

    // Публічний статичний метод для отримання єдиного екземпляра
    public static Authenticator Instance
    {
        get
        {
            // Double-check locking для забезпечення thread-safety
            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new Authenticator();
                    }
                }
            }
            return instance;
        }
    }

    // Метод для аутентифікації користувача
    public bool Authenticate(string username, string password)
    {
        // Реалізація логіки аутентифікації
        return username == "admin" && password == "password";
    }

    // Метод для запису в лог
    public void Log(string message)
    {
        logger.WriteLine($"{DateTime.Now}: {message}");
        logger.Flush();
    }

    ~Authenticator()
    {
        // Закриття логера при знищенні об'єкта
        logger.Close();
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Отримання єдиного екземпляра Authenticator
        Authenticator auth1 = Authenticator.Instance;
        Authenticator auth2 = Authenticator.Instance;

        // Перевірка, що обидві змінні вказують на один і той самий екземпляр
        if (ReferenceEquals(auth1, auth2))
        {
            Console.WriteLine("Both instances are the same.");
        }

        // Використання метода аутентифікації
        bool isAuthenticated = auth1.Authenticate("admin", "password");
        Console.WriteLine($"Authentication result: {isAuthenticated}");

        // Запис у лог
        auth1.Log("User attempted to authenticate.");
    }
}
