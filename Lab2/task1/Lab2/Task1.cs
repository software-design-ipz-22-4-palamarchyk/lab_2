﻿using System;
using System.Collections.Generic;

// Абстрактний клас для підписок
abstract class Subscription
{
    public decimal MonthlyFee { get; set; }
    public int MinPeriod { get; set; }
    public List<string> Channels { get; set; }
    public List<string> Features { get; set; }

    protected Subscription(decimal monthlyFee, int minPeriod, List<string> channels, List<string> features)
    {
        MonthlyFee = monthlyFee;
        MinPeriod = minPeriod;
        Channels = channels;
        Features = features;
    }

    public abstract string GetDetails();
}

// Конкретні класи підписок
class DomesticSubscription : Subscription
{
    public DomesticSubscription() : base(10, 6, new List<string> { "Local News", "Local Sports" }, new List<string> { "Basic Support" })
    {
    }

    public override string GetDetails()
    {
        return $"Domestic Subscription: ${MonthlyFee}/month, Min Period: {MinPeriod} months, Channels: {string.Join(", ", Channels)}, Features: {string.Join(", ", Features)}";
    }
}

class EducationalSubscription : Subscription
{
    public EducationalSubscription() : base(5, 12, new List<string> { "Educational Channel", "Documentaries" }, new List<string> { "Online Classes" })
    {
    }

    public override string GetDetails()
    {
        return $"Educational Subscription: ${MonthlyFee}/month, Min Period: {MinPeriod} months, Channels: {string.Join(", ", Channels)}, Features: {string.Join(", ", Features)}";
    }
}

class PremiumSubscription : Subscription
{
    public PremiumSubscription() : base(20, 3, new List<string> { "Premium Movies", "Premium Sports", "International News" }, new List<string> { "Priority Support", "HD Streaming" })
    {
    }

    public override string GetDetails()
    {
        return $"Premium Subscription: ${MonthlyFee}/month, Min Period: {MinPeriod} months, Channels: {string.Join(", ", Channels)}, Features: {string.Join(", ", Features)}";
    }
}

// Абстрактний клас для фабрики
abstract class SubscriptionFactory  // абстрактний клас SubscriptionFactory
{
    public abstract Subscription CreateSubscription(string subscriptionType);
}

// Конкретні фабрики для створення підписок
class WebSite : SubscriptionFactory  //  WebSite
{
    public override Subscription CreateSubscription(string subscriptionType)
    {
        switch (subscriptionType.ToLower())
        {
            case "domestic":
                return new DomesticSubscription();
            case "educational":
                return new EducationalSubscription();
            case "premium":
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Unknown subscription type");
        }
    }
}

class MobileApp : SubscriptionFactory  //  MobileApp
{
    public override Subscription CreateSubscription(string subscriptionType)
    {
        switch (subscriptionType.ToLower())
        {
            case "domestic":
                return new DomesticSubscription();
            case "educational":
                return new EducationalSubscription();
            case "premium":
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Unknown subscription type");
        }
    }
}

class ManagerCall : SubscriptionFactory  //  ManagerCall
{
    public override Subscription CreateSubscription(string subscriptionType)
    {
        switch (subscriptionType.ToLower())
        {
            case "domestic":
                return new DomesticSubscription();
            case "educational":
                return new EducationalSubscription();
            case "premium":
                return new PremiumSubscription();
            default:
                throw new ArgumentException("Unknown subscription type");
        }
    }
}

// Головний метод програми
class Task1
{
    static void Main(string[] args)
    {
        SubscriptionFactory websiteFactory = new WebSite();
        SubscriptionFactory mobileAppFactory = new MobileApp();
        SubscriptionFactory managerCallFactory = new ManagerCall();

        Subscription domesticSubscriptionWebsite = websiteFactory.CreateSubscription("domestic");
        Subscription educationalSubscriptionMobile = mobileAppFactory.CreateSubscription("educational");
        Subscription premiumSubscriptionManager = managerCallFactory.CreateSubscription("premium");

        Console.WriteLine(domesticSubscriptionWebsite.GetDetails());
        Console.WriteLine(educationalSubscriptionMobile.GetDetails());
        Console.WriteLine(premiumSubscriptionManager.GetDetails());
    }
}
