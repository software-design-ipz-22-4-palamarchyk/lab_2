﻿using System;
using System.Collections.Generic;

public class Virus
{
    public double Weight { get; set; }
    public int Age { get; set; }
    public string Name { get; set; }
    public string Species { get; set; }
    public List<Virus> Children { get; set; }

    public Virus(double weight, int age, string name, string species)
    {
        Weight = weight;
        Age = age;
        Name = name;
        Species = species;
        Children = new List<Virus>();
    }

    // Метод для додавання дитини
    public void AddChild(Virus child)
    {
        Children.Add(child);
    }

    // Метод для клонування віруса разом з його дітьми
    public Virus Clone()
    {
        // Виконуємо поверхневе копіювання для простих властивостей
        Virus clone = (Virus)this.MemberwiseClone();

        // Створюємо новий список для дітей
        clone.Children = new List<Virus>();

        // Клонуємо кожну дитину та додаємо до списку дітей клона
        foreach (Virus child in Children)
        {
            clone.Children.Add(child.Clone());
        }

        return clone;
    }

    public override string ToString()
    {
        return $"Name: {Name}, Species: {Species}, Age: {Age}, Weight: {Weight}, Children count: {Children.Count}";
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Створюємо екземпляри для цілого "сімейства" вірусів
        Virus parent = new Virus(0.5, 2, "ParentVirus", "SpeciesA");
        Virus child1 = new Virus(0.2, 1, "ChildVirus1", "SpeciesA");
        Virus child2 = new Virus(0.3, 1, "ChildVirus2", "SpeciesA");
        Virus grandChild1 = new Virus(0.1, 0, "GrandChildVirus1", "SpeciesA");

        // Створюємо родинні зв'язки
        child1.AddChild(grandChild1);
        parent.AddChild(child1);
        parent.AddChild(child2);

        // Клонуємо вірус-батька
        Virus clonedParent = parent.Clone();

        // Виводимо оригінальні та клоновані віруси для перевірки
        Console.WriteLine("Original Parent Virus:");
        PrintVirusFamily(parent);
        Console.WriteLine("\nCloned Parent Virus:");
        PrintVirusFamily(clonedParent);
    }

    static void PrintVirusFamily(Virus virus, string indent = "")
    {
        Console.WriteLine(indent + virus);
        foreach (Virus child in virus.Children)
        {
            PrintVirusFamily(child, indent + "  ");
        }
    }
}
