﻿using System;
using System.Collections.Generic;

// Інтерфейс для білдера
public interface ICharacterBuilder
{
    ICharacterBuilder SetHeight(double height);
    ICharacterBuilder SetBuild(string build);
    ICharacterBuilder SetHairColor(string hairColor);
    ICharacterBuilder SetEyeColor(string eyeColor);
    ICharacterBuilder SetClothing(string clothing);
    ICharacterBuilder AddInventoryItem(string item);
    Character Build();
}

// Клас персонажа
public class Character
{
    public double Height { get; set; }
    public string Build { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothing { get; set; }
    public List<string> Inventory { get; set; }

    public Character()
    {
        Inventory = new List<string>();
    }

    public void ShowDetails()
    {
        Console.WriteLine($"Height: {Height} cm");
        Console.WriteLine($"Build: {Build}");
        Console.WriteLine($"Hair Color: {HairColor}");
        Console.WriteLine($"Eye Color: {EyeColor}");
        Console.WriteLine($"Clothing: {Clothing}");
        Console.WriteLine("Inventory:");
        foreach (var item in Inventory)
        {
            Console.WriteLine($"- {item}");
        }
    }
}

// Клас-директор для спрощеного створення персонажів
public class CharacterDirector
{
    private ICharacterBuilder _builder;

    public CharacterDirector(ICharacterBuilder builder)
    {
        _builder = builder;
    }

    public void ConstructCharacter()
    {
        _builder
            .SetHeight(180)
            .SetBuild("Athletic")
            .SetHairColor("Brown")
            .SetEyeColor("Blue")
            .SetClothing("Armor")
            .AddInventoryItem("Sword")
            .AddInventoryItem("Shield");
    }

    public Character GetCharacter()
    {
        return _builder.Build();
    }
}

// Клас білдера для героїв
public class HeroBuilder : ICharacterBuilder
{
    private Character _hero = new Character();

    public ICharacterBuilder SetHeight(double height)
    {
        _hero.Height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        _hero.Build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        _hero.HairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        _hero.EyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        _hero.Clothing = clothing;
        return this;
    }

    public ICharacterBuilder AddInventoryItem(string item)
    {
        _hero.Inventory.Add(item);
        return this;
    }

    public Character Build()
    {
        return _hero;
    }
}

// Клас білдера для ворогів
public class EnemyBuilder : ICharacterBuilder
{
    private Character _enemy = new Character();

    public ICharacterBuilder SetHeight(double height)
    {
        _enemy.Height = height;
        return this;
    }

    public ICharacterBuilder SetBuild(string build)
    {
        _enemy.Build = build;
        return this;
    }

    public ICharacterBuilder SetHairColor(string hairColor)
    {
        _enemy.HairColor = hairColor;
        return this;
    }

    public ICharacterBuilder SetEyeColor(string eyeColor)
    {
        _enemy.EyeColor = eyeColor;
        return this;
    }

    public ICharacterBuilder SetClothing(string clothing)
    {
        _enemy.Clothing = clothing;
        return this;
    }

    public ICharacterBuilder AddInventoryItem(string item)
    {
        _enemy.Inventory.Add(item);
        return this;
    }

    public Character Build()
    {
        return _enemy;
    }

    public EnemyBuilder AddEvilDeed(string deed)
    {
        _enemy.Inventory.Add(deed);
        return this;
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Створюємо героя за допомогою HeroBuilder
        CharacterDirector heroDirector = new CharacterDirector(new HeroBuilder());
        heroDirector.ConstructCharacter();
        Character hero = heroDirector.GetCharacter();

        // Створюємо ворога за допомогою EnemyBuilder
        CharacterDirector enemyDirector = new CharacterDirector(new EnemyBuilder());
        enemyDirector.ConstructCharacter();
        Character enemy = enemyDirector.GetCharacter();
        ((EnemyBuilder)new EnemyBuilder()).AddEvilDeed("Burned down a village");

        // Виводимо деталі персонажів
        Console.WriteLine("Hero Details:");
        hero.ShowDetails();
        Console.WriteLine("\nEnemy Details:");
        enemy.ShowDetails();
    }
}
